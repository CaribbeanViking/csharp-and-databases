﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseConnection.Repository
{
    public class ConnectionStringHelper
    {
        /// <summary>
        /// Creates a SqlConnectionStringBuilder object.<para/>
        /// Sets up a connection string that contains information about the database that we wish to connect to.
        /// </summary>
        /// <returns>A SqlConnectionStringBuilder object and the string assosciated to that object.</returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder();
            connectionString.DataSource = @"localhost\SQLEXPRESS";
            connectionString.InitialCatalog = "Chinook";
            connectionString.IntegratedSecurity = true;
            connectionString.Encrypt = false;

            return connectionString.ConnectionString;
        }
    }
}
