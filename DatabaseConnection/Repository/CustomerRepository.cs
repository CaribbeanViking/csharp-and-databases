﻿using DatabaseConnection.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;

namespace DatabaseConnection.Repository
{
    public class CustomerRepository : ICustomerRepo
    {
        /// <summary>
        /// AddNewCustomer attempts to set a new customer to the Customer table in the Chinook database.<para/>
        /// The columns that are effected are: CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email.<para/>
        /// A connection is opened via the connection string and a command is issued that alters certain parameters.<para/>
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>If the insert was successful the function returns true.</returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;

            // SET IDENTIRY_INSERT is called due to the Id column not being set to "can be altered" in the database.
            string sql = "SET IDENTITY_INSERT Customer ON INSERT INTO Customer(CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES(@CustomerId, @FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    // Make a command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // Handle Result

                        command.Parameters.AddWithValue("@CustomerId", customer.Id);
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.PhoneNumber);
                        command.Parameters.AddWithValue("@Email", customer.Email);

                        success = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            return success;
        }

        /// <summary>
        /// Method to get number of customers from each country in the database in descending order.
        /// </summary>
        /// <returns>List of CustomerCountry</returns>
        public List<CustomerCountry> GetCustomersInEachCountry()
        {
            List<CustomerCountry> customerList = new List<CustomerCountry>();
            string sql = "SELECT Country, COUNT(Country) FROM Customer GROUP BY Country ORDER BY COUNT(Country) DESC";

            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // Reader
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry customer = new CustomerCountry();
                                //dynamic customer = new ExpandoObject();
                                //reader.GetString(1) = false ? "0" : "1";

                                // Habdle Result
                                customer.Country = reader.GetString(0);
                                customer.Count = reader.GetInt32(1);
                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customerList;
        }

        //*****//
        #region GetCustomers

        /// <summary>
        /// GetAllCustomers() selects specified columns from the table Customer in the Chinook database.<para/>
        /// Columns selected are CustomerId, FirstName, LastName, Country, PostalCode, Phone and Email.<para/>
        /// Opens a connections with the database via the connection string.<para/>
        /// Reads the data. Sets the data to a new Customer object that is later added to a list of customers.
        /// </summary>
        /// <returns>A list of customers (datatype Customer).</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    // Make a command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // Reader
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                Customer customer = new Customer();

                                // Habdle Result
                                customer.Id = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);

                                // The TWO checks below are to ensure that if there is a null value a value is hardcoded.
                                if (reader[4] == DBNull.Value)
                                    customer.PostalCode = "0";
                                else
                                    customer.PostalCode = reader.GetString(4);

                                if (reader[5] == DBNull.Value)
                                    customer.PhoneNumber = "0";
                                else
                                    customer.PhoneNumber = reader.GetString(5);

                                customer.Email = reader.GetString(6);

                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customerList;
        }

        /// <summary>
        /// Returns 1 customer by their Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A customer object from the db</returns>
        public Customer GetCustomerById(string id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = " + id;

            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    // Make a command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // Reader
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //reader.GetString(1) = false ? "0" : "1";

                                // Handle Result
                                customer.Id = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);

                                if (reader[4] == DBNull.Value)
                                    customer.PostalCode = "Postal 0";
                                else
                                    customer.PostalCode = reader.GetString(4);

                                if (reader[5] == DBNull.Value)
                                    customer.PhoneNumber = "Phone 0";
                                else
                                    customer.PhoneNumber = reader.GetString(5);

                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        /// <summary>
        /// Get Customer or Customers by name, display information.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>A list of Customer</returns>
        public List<Customer> GetCustomerByName(string name)
        {
            List<Customer> customerList = new List<Customer>();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE Firstname LIKE '%" + name + "%'";

            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    // Make a command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // Reader
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();

                                // Handle Result
                                customer.Id = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);

                                if (reader[4] == DBNull.Value)
                                    customer.PostalCode = "Postal 0";
                                else
                                    customer.PostalCode = reader.GetString(4);

                                if (reader[5] == DBNull.Value)
                                    customer.PhoneNumber = "Phone 0";
                                else
                                    customer.PhoneNumber = reader.GetString(5);

                                customer.Email = reader.GetString(6);
                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }

        /// <summary>
        /// Method to get a user defined number of rows from the database with offset from top.
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>List of Customer.</returns>
        public List<Customer> GetLimitedAndOffsetCustomers(string limit, string offset)
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET " + offset + " ROWS FETCH NEXT " + limit + " ROWS ONLY";

            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    // Make a command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // Reader
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                Customer customer = new Customer();
                                //reader.GetString(1) = false ? "0" : "1";

                                // Habdle Result
                                customer.Id = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);

                                if (reader[4] == DBNull.Value)
                                    customer.PostalCode = "0";
                                else
                                    customer.PostalCode = reader.GetString(4);

                                if (reader[5] == DBNull.Value)
                                    customer.PhoneNumber = "0";
                                else
                                    customer.PhoneNumber = reader.GetString(5);

                                customer.Email = reader.GetString(6);
                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customerList;
        }

        #endregion
        //*****//

        /// <summary>
        /// Returns a list of the highest spending customers, ordered by descending value.
        /// </summary>
        /// <returns>List of CustomerSpender.</returns>
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> customerList = new List<CustomerSpender>();
            string sql = "SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName, Invoice.Total  " +
                "FROM Customer " +
                "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                "ORDER BY Invoice.Total DESC;";

            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    // Make a command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // Reader
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender customer = new CustomerSpender();

                                // Habdle Result
                                customer.Id = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Total = reader.GetDecimal(3);

                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customerList;
        }

        /// <summary>
        /// Method to update a Customer's first name. Finds Customer by existing first name by user input.
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="newName"></param>
        /// <returns>Customer</returns>
        public Customer UpdateCustomer(Customer customer, string newName)
        {
            bool success = false;
            string sql = "UPDATE Customer SET FirstName = '" + newName + "' WHERE FirstName = '" + customer.FirstName + "'";

            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    // Make a command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // Handle Result
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        success = command.ExecuteNonQuery() > 0 ? true : false;
                        //Console.WriteLine(command.ExecuteNonQuery().ToString());
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            return GetCustomerByName(newName)[0];
        }

        /// <summary>
        /// MostPopularGenre executes a series of Inner Joins in order to reach specific ids related to a customer and genre.<para/>
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of CustomerGenre.</returns>
        public List<CustomerGenre> MostPopularGenre(string id)
        {
            List<CustomerGenre> customerList = new List<CustomerGenre>();
            string sql = "SELECT Genre.Name, COUNT(Genre.Name) " +
                "FROM ((((Customer INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId) " +
                "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId) " +
                "INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId) " +
                "INNER JOIN Genre ON Track.GenreId = Genre.GenreId) " +
                "WHERE Customer.CustomerId ='" + id + "'" +
                "GROUP BY Genre.Name " +
                "ORDER BY COUNT(Genre.Name) DESC;";

            try
            {
                // Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // Reader
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            int temp = 0;
                            while (reader.Read())
                            {

                                CustomerGenre customer = new CustomerGenre();

                                // Handle Result
                                customer.Id = id;
                                customer.Genre = reader.GetString(0);
                                customer.GenreCount = reader.GetInt32(1);

                                // This check ensures that only the top genres are added to the list. Breaks if the genre has a lower count than the previous.
                                if(temp == customer.GenreCount || temp == 0)
                                {
                                    customerList.Add(customer);
                                }
                                else
                                {
                                    break;
                                }

                                temp = customer.GenreCount;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customerList;
        }
    }
}
