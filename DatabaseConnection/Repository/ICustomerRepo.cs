﻿using DatabaseConnection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseConnection.Repository
{
    public interface ICustomerRepo
    {
        public Customer GetCustomerById(string id);
        public List<Customer> GetCustomerByName(string name);
        public List<Customer> GetAllCustomers();
        public List<Customer> GetLimitedAndOffsetCustomers(string limit, string offset);
        public List<CustomerSpender> GetHighestSpenders();
        public List<CustomerCountry> GetCustomersInEachCountry();
        public bool AddNewCustomer(Customer customer);
        public Customer UpdateCustomer(Customer customer, string lookForName);
        public List<CustomerGenre> MostPopularGenre(string id);

    }
}
