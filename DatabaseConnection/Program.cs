﻿// See https://aka.ms/new-console-template for more information
using DatabaseConnection.Models;
using DatabaseConnection.Repository;

#region Secret
//Console.WriteLine("RIP Gustav <3");
#endregion

ICustomerRepo repo = new CustomerRepository();
Console.WriteLine("Test 1: Read all customers from database.\n");
TestSelectAll(repo);
Console.ReadKey();
Console.WriteLine("Test 2: Read a specific customer from database by ID.\n");
TestSelect(repo);
Console.ReadKey();
Console.WriteLine("Test 3: Read a specific customer from database by name.\n");
TestGetByName(repo);
Console.ReadKey();
Console.WriteLine("Test 4: Return limited number of customers with offset from top.\n");
TestSelectLimitAndOffset(repo);
Console.ReadKey();
Console.WriteLine("Test 5: Add a new customer to database.\n");
TestAddNewCustomer(repo);
Console.ReadKey();
Console.WriteLine("Test 6: Update an existing customer.\n");
TestUpdateCustomer(repo);
Console.ReadKey();
Console.WriteLine("Test 7: Return the number of customers in each country.\n");
TestSelectCustomersInEachCountry(repo);
Console.ReadKey();
Console.WriteLine("Test 8: Return customer who are the highest spenders.\n");
TestHighestSpender(repo);
Console.ReadKey();
Console.WriteLine("Test 9: Return the most popular genre for a given customer by ID.\n");
TestTopGenre(repo);
Console.ReadKey();



static void TestSelectAll(ICustomerRepo repository)
{
    PrintCustomers(repository.GetAllCustomers());
}

static void TestSelect(ICustomerRepo repository)
{
    Console.WriteLine("Enter Id: ");
    string Id = Console.ReadLine();
    PrintCustomer(repository.GetCustomerById(Id));
}

static void TestGetByName(ICustomerRepo repository)
{
    Console.WriteLine("Enter string (partial First name): ");
    string name = Console.ReadLine();
    PrintCustomers(repository.GetCustomerByName(name));
}  

static void TestUpdateCustomer(ICustomerRepo repository)
{
    Console.WriteLine("Enter first name of customer to be updated: ");
    string lookForName = Console.ReadLine();
    Customer customer = repository.GetCustomerByName(lookForName)[0];
    Console.WriteLine("Enter new first name: ");
    string newName = Console.ReadLine();
    PrintCustomer(repository.UpdateCustomer(customer, newName));
}

static void TestSelectLimitAndOffset(ICustomerRepo repository)
{
    Console.WriteLine("Input how many customers you want to show (integer): ");
    string limit = Console.ReadLine();
    Console.WriteLine("Input how many customers from the top you would like to skip/offset:");
    string offset = Console.ReadLine();
    PrintCustomers(repository.GetLimitedAndOffsetCustomers(limit, offset));
}

static void TestAddNewCustomer(ICustomerRepo repository)
{
    Customer cusTest = new Customer();
    List<Customer> customerList = repository.GetAllCustomers();
    int tempLength = customerList.Count + 1;
    cusTest.Id = tempLength;
    Console.WriteLine("Input First Name: ");
    cusTest.FirstName = Console.ReadLine();
    Console.WriteLine("Input Last Name: ");
    cusTest.LastName = Console.ReadLine();
    Console.WriteLine("Input Country Of Origin: ");
    cusTest.Country = Console.ReadLine();
    Console.WriteLine("Input Postal Code: ");
    cusTest.PostalCode = Console.ReadLine();
    Console.WriteLine("Input Phone Number: ");
    cusTest.PhoneNumber = Console.ReadLine();
    Console.WriteLine("Input Email: ");
    cusTest.Email = Console.ReadLine();

    if (repository.AddNewCustomer(cusTest))
    {
        int count = customerList.Count() + 1;
        PrintCustomer(repository.GetCustomerById(count.ToString()));
    }
    else
    {
        Console.WriteLine("DARN");
    }

}

static void TestHighestSpender(ICustomerRepo respository)
{
    PrintHighestSpenders(respository.GetHighestSpenders());
}

static void TestSelectCustomersInEachCountry(ICustomerRepo repository)
{
    PrintCountryCounts(repository.GetCustomersInEachCountry());
}

static void TestTopGenre(ICustomerRepo repository)
{
    Console.WriteLine("Enter the Customer Id (int): ");
    //List<CustomerGenre> temp = new List<CustomerGenre>();
    PrintGenres(repository.MostPopularGenre(Console.ReadLine()));
}

static void PrintCustomers(IEnumerable<Customer> customers)
{
    foreach (Customer customer in customers)
    {
        PrintCustomer(customer);
    }
}

static void PrintCustomer(Customer customer)
{
    Console.WriteLine($"{customer.Id,-4}" + $"{customer.FirstName,-15}" + $"{customer.LastName,-15}" + $"{customer.Country,-15}"
    + $"{customer.PostalCode,-15}" + $"{customer.PhoneNumber,-20}" + $"{customer.Email,-15}");
}

static void PrintHighestSpenders(IEnumerable<dynamic> customers)
{
    foreach (dynamic customer in customers)
    {
        Console.WriteLine($"{customer.Id,-5}" + $"{customer.FirstName,-10}" + $"{customer.LastName,-10}" + $"{customer.Total,-10}");
    }
}

static void PrintGenre(dynamic customer)
{
    Console.WriteLine($"{customer.Id,-10}" + $"{customer.GenreCount,-5}" + $"{customer.Genre}");
}

static void PrintGenres(IEnumerable<dynamic> customers)
{
    foreach (dynamic customer in customers)
    {
        PrintGenre(customer);
    }
}

static void PrintCountryCounts(IEnumerable<dynamic> customers)
{
    foreach (dynamic customer in customers)
    {
        PrintCountryCount(customer);
    }
}

static void PrintCountryCount(dynamic customer)
{
    Console.WriteLine($"{customer.Country,-20}" + $"{customer.Count,-5}");
}
