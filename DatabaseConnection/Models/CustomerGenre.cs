﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseConnection.Models
{
    public class CustomerGenre
    {
        public string Id { get; set; }
        public string Genre { get; set; }
        public int GenreCount { get; set; }

    }
}
