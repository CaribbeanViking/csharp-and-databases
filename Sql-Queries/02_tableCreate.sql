CREATE TABLE Superhero (
    Id int NOT NULL IDENTITY(1,1),
    Superhero_name nvarchar(50) NOT NULL,
    Alias nvarchar(100),
    Origin nvarchar(100),
    PRIMARY KEY (Id)
);
CREATE TABLE Assistant (
    Id int NOT NULL IDENTITY(1,1),
    Assistant_name nvarchar(50) NOT NULL,
    PRIMARY KEY (Id)
);
CREATE TABLE SuperPowers (
    Id int NOT NULL IDENTITY(1,1),
    SuperPower_name nvarchar(50) NOT NULL,
    Description nvarchar(100),
    PRIMARY KEY (Id)
);