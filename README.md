## Release
Version: 1.0
Date: 2022-08-16

## Description
This is a Console Application written in C#. It is used to show functionality in editing an existing SQL database. There are nine tests performed.

The SQL queries corresponding to the assignment in Appendix A are located in the folder Sql-Queries.

## Installation

None.

## Using the application

Run Program.cs and the entire database will be loaded automatically (Test 1). You can then hit 'Enter' to go to the next test and follow the instructions. 

## Help

Thouroghly commented for assistance.

## Contributors

https://gitlab.com/CaribbeanViking
https://gitlab.com/saintgod
https://gitlab.com/oskar.nyman


## Contributing

No one else than above mentioned.

## Known Bugs

-
